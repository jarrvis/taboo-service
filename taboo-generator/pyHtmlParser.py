import configparser
import urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup
import json

class HtmlParser:


    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('props.ini')
        self.taboo_html_generator_path = self.config['html_source']['taboo_generator_url']
        print("Taboo html generator path: ", self.taboo_html_generator_path)

    def generate_taboo_list(self, tries):
        result = {}
        for i in range(tries):
            raw_html = self.fetchHTML(self.taboo_html_generator_path)
            self.fetchTabooWordsFromKarTaboo(raw_html, result)
            if i%50==0:
                print(i)
        return result

    def fetchHTML(self, url):
        req = urllib.request.Request(url)
        response = urllib.request.urlopen(req)
        rawHtml = response.read()
        return rawHtml

    def html2Text(self, htmlData):
        soup = BeautifulSoup(htmlData, "lxml")
        return soup.get_text()

    # Removes HTML markup
    def stripScript(self, rawHtml):
        soup = BeautifulSoup(rawHtml, "lxml")
        cleanedHtml = soup.get_text()
        return cleanedHtml

    # Url specific method to get taboo words
    def fetchTabooWordsFromKarTaboo(self, htmlData, result):
        soup = BeautifulSoup(htmlData, "lxml")
        word = soup.find("span", {"class": "card_top_name"}).text
        if word not in result:
            taboo_words = [taboo_word.text for taboo_word in soup.findAll("div", {"class": "card_tabu_div"})]
            result[word] = taboo_words
        return result

x = HtmlParser()
result = x.generate_taboo_list(1000)

jsonData = json.dumps(result, sort_keys=True, indent=4, separators=(',', ':'), ensure_ascii=False)
print(jsonData)
with open('taboo_feed.json', 'w') as f:
    json.dump(jsonData, f, ensure_ascii=False)